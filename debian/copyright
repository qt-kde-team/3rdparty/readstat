Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ReadStat
Source: https://github.com/WizardMac/ReadStat

Files: *
Copyright: 2012-2019, Evan Miller
License: Expat

Files: src/bin/read_csv/jsmn.c
       src/bin/read_csv/jsmn.h
Copyright: 2010, Serge A. Zaitsev
Comment:
 Copied from from https://github.com/zserge/jsmn
License: Expat

Files: src/CKHashTable.c
       src/CKHashTable.h
Copyright:
           2012, Jean-Philippe Aumasson <jeanphilippe.aumasson@gmail.com>
           2012, Daniel J. Bernstein <djb@cr.yp.to>
Comment:
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 . 
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
License: CC0-1.0
 On Debian systems, the full text of the CC0 1.0 Universal license can be found
 in the file `/usr/share/common-licenses/CC0-1.0'.

Files: debian/*
Copyright: 2021-2022, Pino Toscano <pino@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
